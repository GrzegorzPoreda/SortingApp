# Sorting App

This app sorts up to 10 int numbers provided as main() arguments in ascending order.

Main source file:
- [SortingApp](src/main/java/org/example/SortingApp.java)

There are 4 test files - you can run 3 parametrized test files with - [SortingAppSuiteTest](src/test/java/org/example/SortingAppSuiteTest.java) suite file.
