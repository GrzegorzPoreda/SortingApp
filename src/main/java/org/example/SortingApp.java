package org.example;

import java.util.Arrays;

/**
 * Sorts up to 10 integer values given in the command line in ascending order.
 * @author Grzegorz Poreda
 */
public class SortingApp {

    /**
     * Writes an int array to standard output stream
     */
    public void printIntArray(int[] array) {
        for (int number : array) {
            System.out.print(number + " ");
        }
    }

    /**
     * Sorts an array of integers in ascending order.
     */
    public void sort(int[] array) {
        Arrays.sort(array);
    }

    /**
     * Converts a String array to an int array
     */
    public int[] convertToIntArray(String[] stringArray) {
        if (stringArray.length == 0 | stringArray.length > 10) {
            throw new IllegalArgumentException("Number of arguments must be between 1 and 10!");
        }
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            try {
                int newValue = Integer.parseInt(stringArray[i]);
                intArray[i] = newValue;
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Arguments must be of type int!");
            }
        }
        return intArray;
    }

    public static void main(String[] args) {

        SortingApp sortingApp = new SortingApp();

        int[] numbers = sortingApp.convertToIntArray(args);
        sortingApp.sort(numbers);
        sortingApp.printIntArray(numbers);
    }
}
