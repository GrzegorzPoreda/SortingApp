package org.example;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;



/**
 * Suite file for tests for SortingApp.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        SortingAppAcceptableNumberOfArgsTest.class,
        SortingAppUnacceptableNumberOfArgsTest.class,
        SortingAppWrongTypeOfArgsTest.class
})
public class SortingAppSuiteTest {

}
