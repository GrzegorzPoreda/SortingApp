package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


/**
 * Unit test for SortingApp.
 */
@RunWith(Parameterized.class)
public class SortingAppUnacceptableNumberOfArgsTest {
    protected SortingApp sortingApp = new SortingApp();
    protected String[] array;

    public SortingAppUnacceptableNumberOfArgsTest(String[] array) {
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}},
                {new String[]{"12","3","2","4","1","7","7","4","123","0","14"}},
                {new String[]{"4","1","67","2","78","3","9","2","7","1","15","54","17"}},
                {new String[]{"1","4","34","43","145","256","345","431","547","600","200","1"}},
                {new String[]{"45","41","37","34","29","26","19","14","9","1","45","41","37","34","29","26","19","14"}}
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testUnacceptableNumberOfParametersCase() {
        sortingApp.convertToIntArray(array);
    }
}
