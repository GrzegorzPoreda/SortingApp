package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


/**
 * Unit test for SortingApp.
 */
@RunWith(Parameterized.class)
public class SortingAppWrongTypeOfArgsTest {
    protected SortingApp sortingApp = new SortingApp();
    protected String[] array;

    public SortingAppWrongTypeOfArgsTest(String[] array) {
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"a"}},
                {new String[]{"a","1","3"}},
                {new String[]{"3","4","7","2","0","1","Alice"}}
        });
    }

    @Test (expected = NumberFormatException.class)
    public void testWrongTypeOfParametersCase() {
        sortingApp.convertToIntArray(array);
    }
}
