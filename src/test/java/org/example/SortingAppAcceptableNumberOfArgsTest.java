package org.example;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for SortingApp.
 */
@RunWith(Parameterized.class)
public class SortingAppAcceptableNumberOfArgsTest {
    protected SortingApp sortingApp = new SortingApp();
    protected String[] array;
    protected String expected;
    protected final PrintStream standardOut = System.out;
    protected final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    public SortingAppAcceptableNumberOfArgsTest(String[] array, String expected) {
        this.array = array;
        this.expected = expected;
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1"}, "1"},
                {new String[]{"3"}, "3"},
                {new String[]{"9"}, "9"},
                {new String[]{"12"}, "12"},
                {new String[]{"12","3","2","4","1","7","7","4","123","0"}, "0 1 2 3 4 4 7 7 12 123"},
                {new String[]{"4","1","67","2","78","3","9","2","7","1"}, "1 1 2 2 3 4 7 9 67 78"},
                {new String[]{"1","4","34","43","145","256","345","431","547","600"},
                        "1 4 34 43 145 256 345 431 547 600"},
                {new String[]{"45","41","37","34","29","26","19","14","9","1"}, "1 9 14 19 26 29 34 37 41 45"},
                {new String[]{"4","3","7","1","12"}, "1 3 4 7 12"},
                {new String[]{"1","3","6","9","12","15"}, "1 3 6 9 12 15"},
                {new String[]{"45","39","27","4"}, "4 27 39 45"}
        });
    }

    @Test
    public void testAcceptableNumberOfParametersCase() {
        int[] numbers = sortingApp.convertToIntArray(array);
        sortingApp.sort(numbers);
        sortingApp.printIntArray(numbers);
        assertEquals(expected, outputStreamCaptor.toString().trim());
    }
}
